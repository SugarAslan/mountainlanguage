import Lexer.Lexer;
import Lexer.Token;
import Parser.Parser;
import StackMachine.*;

import java.util.List;
import java.util.Map;
import java.util.Scanner;

//  int a = 0;
//  while( a <= 3 ) {
//  a = a + 1;
//  }
//  print;

//  int a = 0;
//  for(int i = 0; i < 5; i = i + 1){
//  a = a + 2;
//  }
//  print;

//  int a = 0;
//  if(a < 3){
//  a = 10;
//  }
//  print;

public class MountainLanguage {

    public static void main(String[] args) {

        MountainLanguage mountainLanguage = new MountainLanguage();

        mountainLanguage.instruction();

        String code = mountainLanguage.codeScaner();

        mountainLanguage.interpret(code);


    }
    private void instruction(){
        System.out.println();
        System.out.println("Initialize var on craete;");
        System.out.println("No \'--\' or \'++\';");
        System.out.println("Support \'<=\',\'>=\';");
        System.out.println("Type \'print\' to end typing code;");
        System.out.println();
        System.out.println("Example \'while\' loop:");
        System.out.println("int a = 0;\n" +
                "  while( a <= 3 ) {\n" +
                "  a = a + 1;\n" +
                "  }\n" +
                "  print;");
        System.out.println();
        System.out.println("Example \'for\' loop:");
        System.out.println("int a = 0;\n" +
                "  for(int i = 0; i < 5; i = i + 1){\n" +
                "  a = a + 2;\n" +
                "  }\n" +
                "  print;");
        System.out.println();
        System.out.println("Examle \'if\':");
        System.out.println("int a = 0;\n" +
                "  if(a < 3){\n" +
                "  a = 10;\n" +
                "  }\n" +
                "  print;");
        System.out.println();
        System.out.println();
    }

    // Reading code from terminal
    private String codeScaner(){

        Scanner scanner = new Scanner(System.in);
        StringBuffer stringBuffer = new StringBuffer();
        String code = "";

        System.out.println("Start typing code:");
        code = scanner.nextLine();

        while(!code.equals("print;")){
            stringBuffer.append(code);
            code = scanner.nextLine();
        }

        code = stringBuffer.toString();

        System.out.println("Your code:");
        System.out.println(code);
        System.out.println();

        return code;
    }

    private void interpret(String code) {

        // Creating tokens
        Lexer lexer = new Lexer(code);

        Parser parser = new Parser();
        StackMachine stackMachine = new StackMachine();

        List<Token> tokenList = lexer.getTokenList();

        // Looking for errors and create RPN of code
        parser.lang(tokenList);

        // Stack machine start
        Map result = stackMachine.stackCalculation(parser);

        System.out.println("Result of your code:");
        System.out.println(result);
    }


}
